(function($) {
    var resizeWindow = function() {
        var img = 499;
        var w = $(window).width();
        var h = $(window).height();
        var h2 = $('.curte.principal .wrap').height();
        var p = h - h2;
        if (p > 0) {
            p = p / 2;
            if (p < 50) {
                p = 50;
            }
        }
        var img2 = img;
        if (w < 1200) {
            img2 = img * 0.8;
            if (w < 1024) {
                img2 = img * 0.6;
                if (w < 640) {
                    img2 = img * 0.4;
                }
            }
        }
        $('section').height(h);
        $('.curte.principal .wrap').css("padding-top", p + "px");
        $('#img-logo').width(img2);
    };
    $(window).on('load resize', resizeWindow);

    $("#encurtar").submit(function() {
        $("#curtebox").remove();
        $("#copy2clipboard button").attr("title", "Copiar!").removeClass("btn-success").addClass("btn-warning").tooltip("destroy").tooltip({
            placement: "bottom"
        });
        resizeWindow();
        var url = $("#url").val();
        if (url.length > 0) {
            $("#url").attr("readonly", "readonly");
            $("#url_encurtar").text("Aguarde!").attr("disabled", "disabled");
            $.get("index.html", $(this).serialize(), function(data) {
                if (data !== undefined) {
                    console.log(data);
                    var shorturl = data.result.shorturl;

                    $("#curteurl").append('<div id="curtebox"><br /><div class="well"><div id="qrcode"></div><h2 id="encurtado_codigo">' + shorturl + '</h2><div class="btn-toolbar"><div class="btn-group"><button class="btn btn-warning" type="button" id="copy2clipboard" data-clipboard-text="' + shorturl + '" data-original-title="Copiar!">Copiar!</button></div><div class="btn-group"><button class="btn btn-primary" id="share_facebook" type="button" data-original-title="Compatilhar para seus amigos no Facebook!">Facebook</button><button class="btn btn-info" id="share_twitter" type="button" data-original-title="Compatilhar para seus seguidores no Twitter!">Twitter</button><button class="btn btn-danger" id="share_google" type="button" data-original-title="Compatilhar para seus círculos no Google+!">Google+</button></div><div class="zclip" id="zclip-ZeroClipboardMovie_1" style="position: absolute; left: 183px; top: 601px; width: 73px; height: 30px; z-index: 99;"><embed id="ZeroClipboardMovie_1" src="js/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="73" height="30" name="ZeroClipboardMovie_1" align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=1&amp;width=73&amp;height=30" wmode="transparent"></div></div></div></div>');
                    $("#curtebox button").tooltip({
                        placement: "bottom"
                    });

                    var client = new ZeroClipboard(document.getElementById("copy2clipboard"), {
                        moviePath: "js/ZeroClipboard.swf"
                    });
                    client.on("load", function(client) {
                        client.on("complete", function(client, args) {
                            $("#copy2clipboard").attr("title", "Copiado!").removeClass("btn-warning").addClass("btn-success").tooltip("destroy").tooltip({
                                trigger: "hover",
                                placement: "bottom"
                            }).tooltip("toggle");
                        });
                    });

                    $('#qrcode').qrcode({width: 128, height: 128, text: shorturl});

                    $("#share_twitter").click(function() {
                        console.log("share_twitter", $("#encurtado_codigo").text());
                        var titulo = "Eu curti: " + encodeURI($("#encurtado_codigo").text()) + " // " + encodeURI("Curte.eu!");
                        var url = "https://twitter.com/intent/tweet?hashtags=Curte.eu&source=tweetbutton&text=" + titulo; // &original_referer=https%3A%2F%2Ftwitter.com%2Fabout%2Fresources%2Fbuttons
                        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
                    });

                    $("#share_facebook").click(function() {
                        console.log("share_twitter", $("#encurtado_codigo").text());
                        var url = "http://www.facebook.com/sharer.php?u=" + encodeURI($("#encurtado_codigo").text());
                        window.open(url, "", "toolbar=0, status=0, width=650, height=450");
                    });

                    $("#share_google").click(function() {
                        console.log("share_twitter", $("#encurtado_codigo").text());
                        var url = "https://plus.google.com/share?url=" + encodeURI($("#encurtado_codigo").text());
                        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
                    });

                    $("#url").val("");
                }
                $("#url").removeAttr("readonly");
                $("#url_encurtar").removeAttr("disabled").text("Curte.eu!");
            }, "json");
        }
        resizeWindow();
        return false;
    });

    var url = $("#url");
    url.focus();

    if (url.val() !== "") {
        $("#encurtar").submit();
    }
})(jQuery);