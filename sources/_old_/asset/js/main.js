$(document).ready(function() {

    $("#encurtar").submit(function() {
        var url = $("#url").val();
        if (url.length > 0) {

            $("#encurtado").not('.hide').addClass('hide');
            $("#copy2clipboard button").attr("title", "Copiar!").removeClass("btn-success").addClass("btn-warning").tooltip("destroy").tooltip({
                placement: "bottom"
            });


            $("#url").attr("readonly", "readonly");
            $("#url_encurtar").text("Aguarde!").attr("disabled", "disabled");
            $.get("index.html", $(this).serialize(), function(data) {
                if (data !== undefined) {
//                    var title = data.result.title;
//                    title = (title.length > 0 ? title : "");
//                    $("#encurtado_titulo").attr("rel", data.result.title).html(data.result.title);
                    
                    $("#encurtado_codigo").html(data.result.shorturl);
                    $("#copy2clipboard button").attr("rel", data.result.shorturl);
                    
                    $("#encurtado button").tooltip({
                        placement: "bottom"
                    });
                    
                    //$('#chamada-chrome').popover('hide');
                    $("#mensagem").addClass('hide');
                    $("#encurtado").removeClass('hide');

                    $("#copy2clipboard").zclip({
                        path: "asset/js/ZeroClipboard.swf",
                        copy: function() {
                            return $("#copy2clipboard button").attr("rel");
                        },
                        afterCopy: function() {
                            $("#copy2clipboard button").attr("title", "Copiado!").removeClass("btn-warning").addClass("btn-success").tooltip("destroy").tooltip({
                                trigger: "hover",
                                placement: "bottom"
                            }).tooltip("toggle");
                        }
                    });

                    $("#url_original").text(url);
                    $("#url").val("");
                    $("#url").removeAttr("readonly");
                    $("#url_encurtar").removeAttr("disabled").text("Encurtar!");

                }
            }, "json");
        }
        return false;
    });

    $("#share_twitter").click(function() {
        var titulo = "Eu curti: " + encodeURI($("#copy2clipboard button").attr("rel")) + " // " + encodeURI("Curte.eu!");
        var url = "https://twitter.com/intent/tweet?hashtags=Curte.eu&source=tweetbutton&text=" + titulo; // &original_referer=https%3A%2F%2Ftwitter.com%2Fabout%2Fresources%2Fbuttons
        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
    });

    $("#share_facebook").click(function() {
        var url = "http://www.facebook.com/sharer.php?u=" + encodeURI($("#copy2clipboard button").attr("rel"));
        window.open(url, "", "toolbar=0, status=0, width=650, height=450");
    });

    $("#share_google").click(function() {
        var url = "https://plus.google.com/share?url=" + encodeURI($("#copy2clipboard button").attr("rel"));
        window.open(url, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");
    });

//    $('#encurtado').on('hidden', function() {
//        $("#copy2clipboard button").attr("title", "Copiar!").removeClass("btn-success").addClass("btn-warning").tooltip("destroy").tooltip({
//            placement: "bottom"
//        });
//        $("#url").removeAttr("readonly");
//        $("#url_encurtar").removeAttr("disabled");
//        $("#url_encurtar").text("Encurtar!");
//    });

    var url = $("#url").val();
    if (url.length > 0) {
        $("#encurtar").submit();
    }

    if ($('#chamada-chrome').length > 0) {
        $('#chamada-chrome').popover({
            trigger: "hover",
            placement: "bottom",
            content: "Baixa já o aplicativo para Google Chrome no <b>Chrome Web Store</b>!"
        }).popover('hide');
    }

//    setTimeout(function() {
//        $('#chamada-chrome').popover('hide');
//    }, 3000);

});