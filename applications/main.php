<?php

if (!class_exists("pages", false))
    include path::plugins("insigndigital/pages.php");

class main extends pages {

    public function __construct() {
        parent::__construct();
        $this->detect();

        if ($this->sitemap)
            die($this->sSitemap());

        if ($this->f)
            die($this->gApi());

        $this->gRedirect();
    }

    private function gApi() {
        if ($this->url) {
            $this->sHash($this->url);
            if ($this->codigo) {
                $array = array(
                    "url" => array(
                        "keyword" => $this->url,
                        "url" => $this->url,
                        "date" => date("Y-m-d H:i:s"),
                        "ip" => $_SERVER["REMOTE_ADDR"]
                    ),
                    "status" => "success",
                    "message" => "{$this->url} add to database",
                    "title" => false, // $this->titulo
                    "shorturl" => "http://curte.eu/" . $this->codigo,
                    "statusCode" => "200"
                );
                switch ($this->f) {
                    default:
                    case "txt":
                        die("http://curte.eu/" . $this->codigo);
                        break;
                    case "json":
                        $array = array("result" => $array);
                        exit(json_encode($array));
                        break;
                }
            }
        }
    }

    private function gRedirect() {
        $permalink = permalink::get(0);
        if ($permalink) {
            $gHash = $this->gHash($permalink);
            if ($gHash) {
                if ($this->sViews($permalink)) {
                    die(knife::redirect($gHash));
                }
            }
        }
        return false;
    }

    private function sViews($hash = false) {
        $db = new mysqlsave();
        $db->table("url");
        $db->column("visualizacoes", "IF(visualizacoes = 0, 1, visualizacoes + 1)");
        if ($hash) {
            $db->match("codigo", $hash);
            $db->match("apelido", $hash, "OR");
        }
        return $db->go();
    }

    private function sHash($url) {
        if (knife::is_url($url) && !preg_match("/curte\.eu/i", $url)) {
            $informacao = $this->gHash(false, $url);
            if ($informacao) {
                return $this->extract($informacao);
            }
            $hash = substr(sha1(uniqid($url . mt_rand(), true)), 0, 6);
            $informacao = $this->gInfo($url);
            $db = new mysqlsave();
            $db->table("url");
            $db->column("url", $url);
            $db->column("codigo", $hash);
            $db->column("titulo", $informacao["titulo"]);
            $db->column("descricao", $informacao["descricao"]);
            $db->column("keywords", $informacao["keywords"]);
            $db->column("server_info", serialize($_SERVER));
            if ($db->go()) {
                $informacao["codigo"] = $hash;
                return $this->extract($informacao);
            }
        }
        return false;
    }

    private function gMeta($content) {
        $info = array("description" => null, "keywords" => null);
        $content = trim(str_replace(array("\n", "\r", "\t"), "", $content));
        $content = preg_replace("'<style[^>]*>.*</style>'siU", '', $content);  // strip js
        $content = preg_replace("'<script[^>]*>.*</script>'siU", '', $content); // strip css
        if (preg_match_all("/\<meta(.*?)\>/i", $content, $metas)) {
            foreach ($metas[1] as $rows) {
                if (preg_match("/name\=('(.*?)'|\"(.*?)\")/i", $rows, $name)) {
                    $name = strtolower(trim((isset($name[3]) ? $name[3] : $name[2])));
                    if (preg_match("/content\=('(.*?)'|\"(.*?)\")/i", $rows, $content)) {
                        $info[$name] = trim($content[2]);
                    }
                }
            }
        }
        return $info;
    }

    private function gInfoItem($string) {
        $string = (mb_detect_encoding($string) != 'UTF-8' ? utf8_encode($string) : $string);
        $string = utf8_encode(html_entity_decode($string));
        $string = (mb_detect_encoding($string) != 'UTF-8' ? utf8_encode($string) : $string);
        $string = strip_tags($string);
        $string = str_replace(array("\r", "\n"), "", $string);
        $string = trim($string);
        return $string;
    }

    private function gInfo($url) {
        $info = array("titulo" => $url, "descricao" => "Sem informação!", "keywords" => "Sem informação!");
//        $content = knife::open($url);
//        if (preg_match("/<title>([^<]+)<\/title>/i", $content, $match_title)) {
//            if (isset($match_title[1])) {
//                $match_meta = $this->gMeta($content);
//                $info["titulo"] = $this->gInfoItem($match_title[1]);
//                $info["descricao"] = $this->gInfoItem($match_meta["description"]);
//                $info["keywords"] = $this->gInfoItem($match_meta["keywords"]);
//            }
//        }
        return $info;
    }

    private function sSitemap($sSitemap = false, $total = false, $inicial = 0) {
        if (!$inicial and !$total and !$sSitemap) {
            if (!class_exists("sitemap", false))
                include path::plugins("insigndigital/sitemap.php");
            $db = new mysqlsearch();
            $db->table("url");
            $db->column("COUNT(id)", false, "total");
            $total = $db->go();
            $total = $total[0]["total"];
            $sSitemap = new sitemap();
        }
        if ($total > 0 and $inicial < $total) {
            $db = new mysqlsearch();
            $db->table("url");
            $db->column("codigo");
            $db->column("apelido");
            $db->limit($inicial, 1000);
            $urls = $db->go();
            if (is_array($urls)) {
                foreach ($urls as $url) {
                    $sSitemap->url("http://" . domain . "/{$url["codigo"]}", false, "always");
                    if ($url["apelido"]) {
                        $sSitemap->url("http://" . domain . "/{$url["apelido"]}", false, "always");
                    }
                }
            }
            $this->sSitemap($sSitemap, $total, ($inicial + 1000));
        }
        if (!$inicial) {
            return $sSitemap->go();
        }
    }

    private function gHash($hash = false, $url = false) {
        $db = new mysqlsearch();
        $db->table("url");
        if ($hash) {
            $db->column("url");
            $db->match("codigo", $hash);
            $db->match("apelido", $hash, "OR");
        }
        if ($url) {
            $db->column("*");
            $db->match("url", $url);
        }
        $db->limit(1);
        $result = $db->go();
        if ($result) {
            if ($url) {
                return $result[0];
            }
            return $result[0]["url"];
        }
        return false;
    }

}